<?php

namespace App;

use Carbon\Carbon;

class Post extends Model
{
    public function comments() {
        return $this->hasMany(Comment::class);
    }

    public function addComment($body) {

        // Comment::create([
        //     'body' => $body,
        //     'post_id' => $post->id,
        // ]);

        $this->comments()->create([
            'body' => request('body'),
            'user_id' => auth()->id(),
        ]);

    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function scopeFilter($query, $filters) {

        if($filters['year']) {
            $query->whereYear('created_at', $filters['year']);
        }

        if($filters['month']) {
            try {
                $query->whereMonth('created_at', Carbon::parse($filters['month'])->month);
            } catch (\Exception $err) {

            }
        }
    }

    public static function archives() {
        return static::selectRaw('year(created_at) as year, monthname(created_at) as month, count(*) as published')
        ->groupBy('year', 'month')
        ->orderByRaw('min(created_at) DESC')
        ->get()
        ->toArray();
    }

    public function tags() {
        return $this->belongsToMany(Tag::class);
    }
}
