<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PostsController@index')->name('posts_index');
Route::get('/archive/{year}/{month}', 'PostsController@archive')->name('posts_archive');
Route::get('/posts/create', 'PostsController@create')->name('posts_create');
Route::post('/posts', 'PostsController@store')->name('posts_store');
Route::get('/posts/{post}', 'PostsController@show')->name('posts_show');
Route::patch('/posts/{post}/comments', 'CommentsController@store')->name('comments_store');
Route::get('/posts/tags/{tag}', 'TagsController@index')->name('tags_index');


/*

GET /posts
GET /posts/create
POST /posts
GET /posts/{id}
GET /posts/{id}/edit
PATCH /posts/{id}
DELETE /posts/{id}
 */

/* Registration and Authentication routes */

Route::get('/register', 'RegistrationController@create')->name('register');
Route::post('/register', 'RegistrationController@store')->name('register_store');

Route::get('/login', 'SessionController@create')->name('login');
Route::post('/login', 'SessionController@store')->name('login_store');

Route::get('/logout', 'SessionController@destroy')->name('logout');