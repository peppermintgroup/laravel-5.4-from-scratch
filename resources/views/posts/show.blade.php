@extends('layouts.master')

@section('content')
    <div class="col-sm-8 blog-main">

        @include('posts.post')

        @if(Auth::check())
            @include('posts.add_comment')
        @endif

    </div><!-- /.blog-main -->
@endsection