@foreach($posts as $post)

    <div class="blog-post">
        <h2 class="blog-post-title">
           <a href="{{ route('posts_show', [$post->id]) }}">{{ $post->title }}</a>
        </h2>

        <p class="blog-post-meta">
            {{ $post->created_at->diffForHumans() }} <strong>{{ $post->user->name }}</strong>: &nbsp;
        </p>

        <p>
            {{ $post->body }}
        </p>

        <hr>
    </div><!-- /.blog-post -->

@endforeach