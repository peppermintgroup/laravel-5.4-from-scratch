<div class="blog-post">
    <h2 class="blog-post-title">
       {{ $post->title }}
    </h2>

    <p class="blog-post-meta">
        {{ $post->created_at->diffForHumans() }} <strong>{{ $post->user->name }}</strong>: &nbsp;
    </p>

    <p>
        {{ $post->body }}
    </p>

    <hr>


    <div class="comments">
        <ul class="list-group">
            @foreach ($post->comments as $comment)
                <li class="list-group-item">
                    {{ $comment->user->name }}&nbsp;
                    <strong>
                        [{{ $comment->created_at->diffForHumans() }}]
                    </strong>: &nbsp;

                    {{ $comment->body }}
                </li>
            @endforeach
        </ul>
    </div>
</div><!-- /.blog-post -->
