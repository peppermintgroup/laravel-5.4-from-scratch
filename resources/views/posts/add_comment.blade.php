<div class="card">
    <div class="card-block">
        <form method="POST" action="/posts/{{ $post->id }}/comments">
            {{ method_field('PATCH') }}
            {{ csrf_field() }}
            <div class="form-group">
                <textarea name="body" placeholder="You comment here." class="form-control" required></textarea>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Add Comment</button>
            </div>

            @include('layouts.errors')
        </form>
    </div>
</div>